/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package funciones_p31;
import java.util.Scanner;

/**
 *
 * @author droa
 */
public class Funciones_P31 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner leer = new Scanner(System.in);
        
        System.out.println("Ingrese una Frase:");
        String frase = leer.nextLine();
        System.out.println("Texto codificado es: "+codificar(frase));
        
        
        
        
    }
    public static String codificar(String cadena){
        
        int l=cadena.length();  // Equivalente longitud() -> PSEINT
        String cadena_cod="";
        
        for (int i=0;i<l;i++) {
            
            char letra=cadena.charAt(i); //Equivalente a Subcadena() -> PSEINT
            
            switch (letra) {
                case 'a' -> letra='@';
                case 'e' -> letra='#';
                case 'i' -> letra='$';
                case 'o' -> letra='%';
                case 'u' -> letra='*';
                                   
            }
            cadena_cod=cadena_cod.concat(String.valueOf(letra));
                
            
        }
        
        
        return cadena_cod;
    }
}
