/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estructuras_p27;

import java.util.Scanner;

/**
 *
 * @author droa
 */
public class Estructuras_P27 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner leer = new Scanner(System.in);
        
        
        int contador=0; //Control 20 numeros
        int num=0;
        int sumatoria=0;
        
        do {
            System.out.println("Ingrese un número;");
            num = leer.nextInt();
            contador++;
            if(num>0){
                sumatoria=sumatoria+num;
            }
            if(num==0){
                System.out.println("Se capturó el número cero.");
                break;
            }
        } while (contador<20 && num!=0);
        System.out.println("Se ha ingresado 20 números. La Sumatoria de ellos es: "+sumatoria);
    }
    
}
